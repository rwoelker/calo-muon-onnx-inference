#include <vector>
#include <algorithm>
#include <cmath>

std::vector<float> get_bins(float, float, int);

float get_median(std::vector<float>);

int get_bin(std::vector<float> &, float &);

int channel_for_sampling_id(int &);

const std::vector<float> unwrapPhiAngles(const std::vector<float> &);

std::vector<float> unwrapVector(const std::vector<float> &);

// function copied from here
// https://gitlab.cern.ch/gabarone/athena/-/blob/CaloTagsMMC/Event/FourMomUtils/FourMomUtils/xAODP4Helpers.h#L69
inline double deltaPhi(double phiA, double phiB)
{
    return -remainder(-phiA + phiB, 2 * M_PI);
}
