#include "CaloMuonScoreToolHelper.h"
#include <iostream>
#include <cmath>

std::vector<float> get_bins(float min, float max, int n_bins)
{

    float h = (max - min) / static_cast<float>(n_bins - 1);
    std::vector<float> xs(n_bins);
    std::vector<float>::iterator x;
    float val;
    for (x = xs.begin(), val = min; x != xs.end(); ++x, val += h)
        *x = val;

    return xs;
}

// float get_median(std::vector<float> vec)
// {
//     std::nth_element(vec.begin(), vec.begin() + vec.size() / 2, vec.end());

//     float val_right = *(vec.begin() + (vec.size() / 2));

//     if (vec.size() % 2 == 1)
//     {
//         return val_right;
//     }

//     float val_left = *(vec.begin() + (vec.size() / 2 - 1));

//     return (val_left + val_right) / 2;
// }
float get_median(std::vector<float> v)
{
    if (v.empty())
        return 0.0;

    int n = v.size() / 2;
    std::nth_element(v.begin(), v.begin() + n, v.end());
    float med = v[n];

    if (v.size() % 2 == 1)
        return med;

    auto max_it = std::max_element(v.begin(), v.begin() + n);
    return (*max_it + med) / 2.0;
}

// float get_median(std::vector<float> vec)
// {
//         typedef std::vector<int>::size_type vec_sz;

//         vec_sz size = vec.size();

//         std::sort(vec.begin(), vec.end());

//         vec_sz mid = size/2;

//         return size % 2 == 0 ? (vec[mid] + vec[mid-1]) / 2 : vec[mid];
// }

int get_bin(std::vector<float> &bins, float &val)
{
    // return -1 if value is outside the range
    if (val < bins.front())
    {
        return -1;
    }

    if (val > bins.back())
    {
        return -1;
    }

    for (int i = 0; i < bins.size(); i++)
    {
        if (val < bins[i])
            return i;
    }

    return -1;
}

int channel_for_sampling_id(int &sampling_id)
{
    //[0,1,2,3,12,13,14]
    switch (sampling_id)
    {
    case 0:
        return 0;
    case 1:
        return 1;
    case 2:
        return 2;
    case 3:
        return 3;
    case 12:
        return 4;
    case 13:
        return 5;
    case 14:
        return 6;
    default:
        return -1;
    }
}

// d = d > pi ? d - 2 * pi : (d < -pi ? d + 2 * pi : d);

// std::vector<float> unwrapVector(const std::vector<float> &in)
// {
//     std::vector<float> out(in.size());

//     out[0] = in[0];

//     for (int i = 1; i < out.size(); i++)
//     {
//         float d = deltaPhi(in[i], in[i - 1]);
//         out[i] = out[i - 1] + d;
//     }

//     return out;
// }

std::vector<float> unwrapVector(const std::vector<float> &in)
{
    std::vector<float> out(in);

    for (float &phi : out)
        phi = fmod(phi + 2 * M_PI, 2 * M_PI);

    return out;
}