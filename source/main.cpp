//
//  main.cpp
//  CaloMuonInference
//
//  Created by Ricardo Woelker on 2020-07-14.
//  Copyright © 2020 ric. All rights reserved.
//

#include <limits>
#include <iostream>
#include <sstream>
#include <cmath>
#include "core/session/onnxruntime_cxx_api.h"
#include <vector>
#include <fstream>
#include "calo_muon.h"
#include <unistd.h>
#include <string>
#include "CaloMuonScoreToolHelper.h"

// check if particle is in trackEta requirement
bool satisfiesTrackEtaCut(float trackEta, float cut = 0.1)
{
  return std::abs(trackEta) <= cut;
}

float runInference(std::vector<float> &tensor)
{
  // initialise session

  Ort::Env env(ORT_LOGGING_LEVEL_ERROR);
  Ort::SessionOptions session_options;

  Ort::AllocatorWithDefaultOptions allocator;

  session_options.SetIntraOpNumThreads(1);
  session_options.SetGraphOptimizationLevel(GraphOptimizationLevel::ORT_ENABLE_EXTENDED);

  m_session = std::make_unique<Ort::Session>(env, MODEL_NAME, session_options);

  // std::cout << "Created ONNX runtime session" << std::endl;

  std::vector<int64_t> input_node_dims;
  size_t num_input_nodes = m_session->GetInputCount();
  std::vector<const char *> input_node_names(num_input_nodes);

  for (std::size_t i = 0; i < num_input_nodes; i++)
  {
    // print input node names
    char *input_name = m_session->GetInputName(i, allocator);
    if (print_node_info)
      std::cout << "Input " << i << " : "
                << " name= " << input_name << std::endl;

    input_node_names[i] = input_name;
    // print input node types
    Ort::TypeInfo type_info = m_session->GetInputTypeInfo(i);
    auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
    ONNXTensorElementDataType type = tensor_info.GetElementType();
    if (print_node_info)
      std::cout << "Input " << i << " : "
                << " type= " << type << std::endl;

    // print input shapes/dims
    input_node_dims = tensor_info.GetShape();
    if (print_node_info)
      std::cout << "Input " << i << " : num_dims= " << input_node_dims.size() << std::endl;
    for (std::size_t j = 0; j < input_node_dims.size(); j++)
    {
      if (input_node_dims[j] < 0)
        input_node_dims[j] = 1;
      if (print_node_info)
        std::cout << "Input" << i << " : dim " << j << "= " << input_node_dims[j] << std::endl;
    }
  }
  if (print_node_info)
    std::cout << std::endl;
  //output nodes
  std::vector<int64_t> output_node_dims;
  size_t num_output_nodes = m_session->GetOutputCount();
  std::vector<const char *> output_node_names(num_output_nodes);

  for (std::size_t i = 0; i < num_output_nodes; i++)
  {
    // print output node names
    char *output_name = m_session->GetOutputName(i, allocator);
    if (print_node_info)
      std::cout << "Output " << i << " : "
                << " name= " << output_name << std::endl;
    output_node_names[i] = output_name;

    Ort::TypeInfo type_info = m_session->GetOutputTypeInfo(i);
    auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
    ONNXTensorElementDataType type = tensor_info.GetElementType();
    if (print_node_info)
      std::cout << "Output " << i << " : "
                << " type= " << type << std::endl;

    // print output shapes/dims
    output_node_dims = tensor_info.GetShape();
    if (print_node_info)
      std::cout << "Output " << i << " : num_dims= " << output_node_dims.size() << std::endl;
    ;
    for (std::size_t j = 0; j < output_node_dims.size(); j++)
    {
      if (output_node_dims[j] < 0)
        output_node_dims[j] = 1;
      if (print_node_info)
        std::cout << "Output" << i << " : dim " << j << "= " << output_node_dims[j] << std::endl;
    }
  }

  // create input tensor object from data values
  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);
  int input_tensor_size(ETA_BINS * PHI_BINS * N_CHANNELS);

  //  input_tensor_values = tensor[m_testSample];
  //  std::cout << *(tensor.data()) << " " << tensor[0] << " " << input_tensor_size << std::endl;
  Ort::Value input_tensor = Ort::Value::CreateTensor<float>(memory_info, tensor.data(), input_tensor_size, input_node_dims.data(), input_node_dims.size());
  assert(input_tensor.IsTensor());
  // std::cout << "check session Run options " << input_node_names.data() << " " << input_node_names.size() << " " << output_node_names.data() << " " << output_node_names.size() << std::endl;
  // std::cout << *(input_node_names.data()) << " " << *(output_node_names.data());
  // score model & input tensor, get back output tensor
  auto output_tensors = m_session->Run(Ort::RunOptions{nullptr}, input_node_names.data(), &input_tensor, input_node_names.size(), output_node_names.data(), output_node_names.size());
  assert(output_tensors.size() == 1 && output_tensors.front().IsTensor());

  // Get pointer to output tensor float values
  float *floatarr = output_tensors.front().GetTensorMutableData<float>();

  // Binary classification - the score is just the first element of the output tensor
  float output_score = floatarr[0];

  // std::cout << "Have output score: " << output_score << std::endl;

  // OrtReleaseValue(output_tensor);
  // OrtReleaseValue(input_tensor);
  return output_score;
}

void printInputMatrix(std::vector<std::vector<float>> &vv)
{
  std::streamsize ss = std::cout.precision();
  std::cout.precision(2);
  float sum = 0.;
  std::cout << "\t";
  for (int eta_bin = 0; eta_bin < 30; eta_bin++)
    std::cout << eta_bin << "\t";
  std::cout << std::endl;
  for (int phi_bin = 0; phi_bin < 30; phi_bin++)
  { // loop over rows
    std::cout << phi_bin << "\t";
    for (int eta_bin = 0; eta_bin < 30; eta_bin++)
    { // loop over columns
      float val = vv[phi_bin][eta_bin];
      std::cout << std::fixed << val << "\t";
      sum += val;
    }
    std::cout << std::endl;
  }

  std::cout.precision(ss);
  std::cout << "have sum " << sum << std::endl;
}

void printInputMatrix2(std::vector<std::vector<float>> &vv)
{
  // std::streamsize ss = std::cout.precision();
  // std::cout.precision(2);
  float sum = 0.;
  float row_sum = 0.;
  std::cout << " ";
  for (int eta_bin = 0; eta_bin < 30; eta_bin++)
    std::cout << eta_bin << "";
  std::cout << std::endl;
  for (int phi_bin = 0; phi_bin < 30; phi_bin++)
  { // loop over rows
    std::cout << phi_bin << "";
    row_sum = 0.;
    for (int eta_bin = 0; eta_bin < 30; eta_bin++)
    { // loop over columns
      float val = vv[phi_bin][eta_bin];
      int printval = val > 0 ? 1 : 0;
      std::cout << val << " ";
      sum += val;
      row_sum += val;
    }
    std::cout << "\trow sum "<<row_sum;
    std::cout << std::endl;
  }

  // std::cout.precision(ss);
  std::cout << "have sum " << sum << std::endl;
}

void printVec(std::vector<int> &v)
{
  std::cout << "Printing vector of length " << v.size() << std::endl;
  for (int i = 0; i < v.size(); i++)
  {
    std::cout << v[i] << "\n";
  }
  std::cout << std::endl;
}

void printVec(std::vector<float> &v)
{
  std::cout << "Printing vector of length " << v.size() << std::endl;
  for (int i = 0; i < v.size(); i++)
  {
    std::cout << v[i] << "\n";
  }
  std::cout << std::endl;
}
// std::vector<float> phiAnglesZeroToTwoPi(std::vector<float> v)
// {
//   std::vector<float> outVector;
//   for (int i = 0; i < v.size(); i++)
//   {
//     float angle = v[i];
//     float element = angle - twopi * floor(angle / twopi);
//     outVector.push_back(element);
//   }
//   return outVector;
// }

// for a given particle, consume vectors for eta, phi, energy, sampling ID
std::vector<float> getInputVector(std::vector<float> &eta, std::vector<float> &phi, std::vector<float> &energy, std::vector<int> &sampling)
{
  int n_cells = eta.size();

  std::vector<float> unwrappedPhi = unwrapVector(phi);

  // std::cout << phi[0] << std::endl;
  float median_eta = get_median(eta);
  float median_phi = get_median(unwrappedPhi);

  std::cout << "Have median phi " << median_phi << std::endl;
  std::cout << "Have median eta " << median_eta << std::endl;

  std::vector<float> tensor(ETA_BINS * PHI_BINS * N_CHANNELS, 0.);

  std::vector<float> eta_bins = get_bins(-ETA_CUT, ETA_CUT, ETA_BINS);
  std::vector<float> phi_bins = get_bins(-PHI_CUT, PHI_CUT, PHI_BINS);

  // first dimension is eta, second dimension is phi
  std::vector<float> zeros(30, 0.);
  std::vector<std::vector<float>> vv(30, zeros);

  int n_unacceptable = 0;

  std::vector<float> shifted_phis, shifted_etas;
  std::cout << "have n cells " << n_cells << std::endl;
  for (int i = 0; i < n_cells; i++)
  {

    // take eta and phi values, and shift them by their repsective median
    float _eta = eta[i] - median_eta;
    float _phi = unwrappedPhi[i] - median_phi;

    shifted_phis.push_back(_phi);
    shifted_etas.push_back(_eta);

    int eta_bin = get_bin(eta_bins, _eta);
    int phi_bin = get_bin(phi_bins, _phi);

    // the cell lies outside the acceptable range
    if (eta_bin == -1 || phi_bin == -1)
    {
      n_unacceptable += 1;
      std::cout << "bin failure " << i << " " << _eta << " " << eta_bin << " " << _phi << " " << phi_bin << std::endl;
      continue;
    }

    int channel = channel_for_sampling_id(sampling[i]);

    // this really should not happen
    if (channel == -1)
    {
      n_unacceptable += 1;
      // std::cout << "CHANNEL FAILURE!!! " << i << std::endl;
      continue;
    }

    // 3D array flattening in row-major style
    int tensor_idx = eta_bin * PHI_BINS * N_CHANNELS + phi_bin * N_CHANNELS + channel;

    tensor[tensor_idx] += energy[i];
    if (channel == 0)
    {
      // phi bin is the row
      // eta bin is the column
      // std::cout << i << " row " << phi_bin << " column " << eta_bin << " value " << energy[i] << std::endl;
      vv[phi_bin][eta_bin] += energy[i];
    }
  }

  // std::cout<<"shifted phis "<< std::endl;
  // printVec(shifted_phis);
  std::cout << "have unacceptable entries " << n_unacceptable << std::endl;
  printInputMatrix2(vv);
  return tensor;
}

std::tuple<std::vector<float>, std::vector<float>, std::vector<float>, std::vector<int>, std::vector<int>, std::vector<float>> read_csv(std::string fname = "./data/event_0.csv")
{
  // open csv file
  std::ifstream myFile(fname);

  std::vector<std::string> classData;
  std::vector<float> eta, phi, energy, track_eta, scores;
  std::vector<int> sampling, numbers;
  std::string line;

  while (getline(myFile, line))
  {
    std::stringstream ss(line);
    std::string data;
    int step = 0;
    while (getline(ss, data, ','))
    {
      if (step == 0)
      {
        eta.push_back(std::stof(data));
        step++;
        continue;
      }

      if (step == 1)
      {
        phi.push_back(std::stof(data));
        step++;
        continue;
      }

      if (step == 2)
      {
        energy.push_back(std::stof(data));
        step++;
        continue;
      }

      if (step == 3)
      {
        sampling.push_back(std::stoi(data));
        step++;
        continue;
      }

      if (step == 4)
      {
        track_eta.push_back(std::stof(data));
        step++;
        continue;
      }

      if (step == 5)
      {
        numbers.push_back(std::stoi(data));
        step++;
        continue;
      }

      scores.push_back(std::stof(data));

      step = 0;
    }
  }
  // std::cout << "have results " << eta[0] << " " << phi[0] << " " << energy[0] << " " << sampling[0] << " " << track_eta[0] << std::endl;
  return std::make_tuple(eta, phi, energy, sampling, numbers, scores);
}

int main(int argc, const char *argv[])
{
  // float f = 7.;
  // float eps = f / 3 - 4. / 3 - 1;
  // std::cout << "Float epsilon is " << eps << std::endl;

  // double f1 = 7.;
  // double eps1 = f1 / 3 - 4. / 3 - 1;
  // std::cout << "Double epsilon is " << eps1 << std::endl;

  typedef std::numeric_limits<double> dbl;

  int nevents = std::atoi(argv[2]);
  std::string basename = argv[1];
  int runOnly = std::atoi(argv[3]);
  std::cout.precision(10);
  std::cout << "\nRunning on base " << basename << " and events: " << nevents << " running only " << runOnly << "\n\n";

  std::vector<float> scores, true_scores;
  std::vector<int> event_numbers;
  std::vector<float> first_etas;

  std::cout << "testvec of size " << testv2.size() << " median " << get_median(testv2) << std::endl;
  std::vector<float> v_phis;
  for (int i = 0; i < nevents; i++)

  {
    if (runOnly > -1 && i != runOnly)
      continue;
    // std::cout << "in event " << i << std::endl;

    std::vector<float> eta, phi, energy, _true_scores;
    std::vector<int> sampling, numbers;

    std::string fname = basename + "/event_" + std::to_string(i) + ".csv";

    std::tie(eta, phi, energy, sampling, numbers, _true_scores) = read_csv(fname);
    // std::cout<<"have true scores "<<_true_scores[0]<<std::endl;
    std::vector<float> input_vector = getInputVector(eta, phi, energy, sampling);
    v_phis.push_back(phi[0]);
    float score = runInference(input_vector);
    scores.push_back(score);
    event_numbers.push_back(numbers.at(0));
    first_etas.push_back(eta.at(0));
    true_scores.push_back(_true_scores[0]);
  }

  int n_same = 0;
  int n_loop = (runOnly != -1) ? 1 : nevents;
  // std::cout << "\n[";
  for (int i = 0; i < n_loop; i++)
  {
    float delta = scores[i] - true_scores[i];
    bool score_is_the_same = std::abs(delta) < 1e-6;
    n_same += score_is_the_same;
    if (!score_is_the_same)
    {
      std::cout << "event " << i << " phi " << v_phis[i] << " score " << scores[i] << " delta " << delta << std::endl;
      // std::cout << "(" << v_phis[i] << ", " << delta << "), ";
    }
    // std::cout << i << " event " << event_numbers.at(i) << "\tscore " << scores.at(i) << "\ttrue score " << true_scores.at(i) << "\tsame " << score_is_the_same << "\tdelta " << delta << " first eta " << first_etas.at(i) << std::endl;
  }
  // std::cout << "]\n";

  std::cout << "\nThe same: " << n_same << "/" << nevents << "=" << float(n_same) / nevents * 1e2 << "%" << std::endl;

  // m_session.reset();
  return 0;
}
