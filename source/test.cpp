#include <iostream>
#include <vector>
#include <algorithm>
#include "test.h"
#include <cassert>

float get_median(std::vector<float> v)
{
  if (v.empty())
    return 0.0;

  int n = v.size() / 2;
  std::nth_element(v.begin(), v.begin() + n, v.end());
  float med = v[n];

  if (v.size() % 2 == 1)
    return med;

  auto max_it = std::max_element(v.begin(), v.begin() + n);
  return (*max_it + med) / 2.0;
}
void test1()
{
  float med = get_median(testvector);
  assert(med == 0.);
}

void test2(){
}

int main()
{
  // test1();
  // test2();
  // std::cout << "This is the whole thing." << std::endl;
  // return 1;
  return -999;
}